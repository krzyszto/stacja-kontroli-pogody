#ifndef I2C_H_
#define I2C_H_
#include <inttypes.h>
#include "usart.h"
#define HSI_VALUE 16000000U     /* 16 MHz */
const int PRZEDAWNIENIE;
const int32_t SUKCES;
const int32_t TIMEOUT_ERR;
#define PODAJ_REJESTR_LUB_RETURN(ADRES, REJESTR, KOM, TXT) do {	  \
		int32_t alamakotaijestbardzozla_TO_JEST_KOD_BLEDU; \
		alamakotaijestbardzozla_TO_JEST_KOD_BLEDU \
			= daj_zawartosc_rejestru(ADRES, REJESTR, KOM); \
		if (alamakotaijestbardzozla_TO_JEST_KOD_BLEDU != SUKCES) { \
			usart_nadaj_blad(TXT, \
			                 alamakotaijestbardzozla_TO_JEST_KOD_BLEDU); \
			return alamakotaijestbardzozla_TO_JEST_KOD_BLEDU; \
		} \
	} while (0)

#define PODAJ_DWA_BAJTY_LUB_RETURN(ADRES, REJESTR, KOM, TXT) do {	  \
		int32_t alamakotaijestbardzozla_TO_JEST_KOD_BLEDU; \
		alamakotaijestbardzozla_TO_JEST_KOD_BLEDU \
			= daj_dwa_bajty_z_rejestru(ADRES, REJESTR, KOM); \
		if (alamakotaijestbardzozla_TO_JEST_KOD_BLEDU != SUKCES) { \
			usart_nadaj_blad(TXT, \
			                 alamakotaijestbardzozla_TO_JEST_KOD_BLEDU); \
			return alamakotaijestbardzozla_TO_JEST_KOD_BLEDU; \
		} \
	} while (0)

#define WPISZ_DO_REJESTRU_LUB_RETURN(ADRES, REJESTR, DANE, TXT) do {	  \
		int32_t alamakotaijestbardzozla_TO_JEST_KOD_BLEDU; \
		alamakotaijestbardzozla_TO_JEST_KOD_BLEDU \
			= wpisz_do_rejestru(ADRES, REJESTR, DANE); \
		if (alamakotaijestbardzozla_TO_JEST_KOD_BLEDU != SUKCES) { \
			usart_nadaj_blad(TXT, \
			                 alamakotaijestbardzozla_TO_JEST_KOD_BLEDU); \
			return alamakotaijestbardzozla_TO_JEST_KOD_BLEDU; \
		} \
	} while (0)

extern void ustaw_i2c(const unsigned int czestotliwosc);

/* adres akcelerometru to LIS35DE_ADDR */
extern int32_t daj_zawartosc_rejestru(const uint8_t adres,
                                      const uint8_t rejestr,
                                      int8_t *const wyn);

extern int32_t wpisz_do_rejestru(const uint8_t adres,
                                 const uint8_t rejestr,
                                 const uint8_t wartosc);

extern int32_t daj_dwa_bajty_z_rejestru(const uint8_t adres,
                                        const uint8_t rejestr,
                                        int16_t *const wyn);

#endif
