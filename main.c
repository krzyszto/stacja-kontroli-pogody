/* zaimplementowac prototyp malej stacji meteorologicznej zbierajacej nastepujace dane: */
/* temperatura */
/* cisnienie */
/* wilgotnosc */
/* oswietlenie */
/* harmonogram pomiarow ma byc sterowany za pomoca rtc */
/* konfigurowanie i odbieranie zebranych danych ma sie odbywac za pomoca rs-232 */
/* pomiedzy pomiarami urzadzenie powinno wchodzic w stan uspienia */
/* jako dodatkowa funkcjonalnosc urzadzenie moze monitorowac stan baterii */
#include <stm32.h>
#include "i2c.h"
#include "diodki.h"
#include "nozki.h"
#include "usart.h"
#include "obsluga-czujnikow.h"


void USART2_IRQHandler(void)
{
	uint8_t otrzymano;
	int16_t wilg, temp;
	int32_t err, cis;
	if ((USART2->SR & USART_SR_RXNE) == 0) {
		usart_nadaj("oszukano mnie");
	}
	otrzymano = USART2->DR + 1;
	nadaj_znak(otrzymano);
	usart_nadaj("cisnienie to");
	err = podaj_cisnienie(&cis);
	if (err != SUKCES) {
		usart_nadaj_blad("nie mozna podac cis", err);
	} else {
		usart_nadaj_liczbe_32(cis);
	}
	usart_nadaj("temperatora to");
	err = podaj_temperature(&temp);
	if (err != SUKCES) {
		usart_nadaj_blad("nie mozna podac temp", err);
	} else {
		usart_nadaj_liczbe_32((int32_t) temp);
	}
	usart_nadaj("wilgotnosc to");
	err = podaj_wilgotnosc(&wilg);
	if (err != SUKCES) {
		usart_nadaj_blad("nie mozna podac wilg", err);
	} else {
		usart_nadaj_liczbe_32((int32_t) wilg);
	}
}

int main(void)
{
	int i = 10000;
	int wyn;
	usart_ustaw();
	usart_nadaj("ala ma kota");
	ustaw_przerwania_usarta();
	ustaw_i2c(HSI_VALUE);
	usart_nadaj_liczbe_32(I2C1->SR1);
	while (i--) {__NOP();}
	do {
		wyn = zainicjalizuj_peryferia();
	} while (wyn != SUKCES);
	zainicjalizuj_peryferia();
	for(;;) {
	}
}
