#include "obsluga-czujnikow.h"
#include "i2c.h"
#include "usart.h"
#include <stdint.h>
#include <stdbool.h>
#include <stm32.h>
#define ZWORKOWY_MODYFIKATOR_ADRESU 0

const uint8_t ADRES_HIGROMETRU = (0xBE + ZWORKOWY_MODYFIKATOR_ADRESU) >> 1;
const uint8_t ADRES_CZUJNIKA_OSWIETLENIA = (0x72 + ZWORKOWY_MODYFIKATOR_ADRESU) >> 1;
const uint8_t ADRES_BAROMETRU = (0xBA + ZWORKOWY_MODYFIKATOR_ADRESU) >> 1;
const uint8_t ADRES_TERMOMETRU = (0x90 + ZWORKOWY_MODYFIKATOR_ADRESU) >> 1;
const uint8_t ADRES_REJESTRU_CONF_TEMP = 0x01;
const uint8_t CTRL_REG1_ADDR_BAROMETRU = 0x20;
const uint8_t CTRL_REG1_HIGROMETRU = 0x20;
const uint8_t CTRL_REG2_HIGROMETRU = 0x21;

/* static int32_t interpolacja(int16_t x0, int16_t y0, */
/*                         int16_t x1, int16_t y1, */
/*                         int32_t pomiar) */
/* { */
/* 	int16_t a = (y1 - y0) / (x1 - x0); */
/* 	int16_t b = -a * x0 + y0; */
/* 	return a * pomiar + b; */
/* } */

static int32_t wlacz_higrometr(void)
{
	int32_t wyn;
	/* power up */
	wyn = wpisz_do_rejestru(ADRES_HIGROMETRU, CTRL_REG1_HIGROMETRU, 0x80);
	if (wyn != SUKCES) {
		usart_nadaj_blad("nie mozna wlaczyc higrometru, I2C1->SR1 = ",
		                 (int32_t) wyn);
		return wyn;
	}
	/* one shot */
	wyn = wpisz_do_rejestru(ADRES_HIGROMETRU, CTRL_REG2_HIGROMETRU, 0x01);
	if (wyn != SUKCES) {
		usart_nadaj_blad("nie mozna ustawic higrometru na one shot, "
		                 "I2C1->SR1 = ", wyn);
	}
	return wyn;
}

static int32_t wylacz_higrometr(void)
{
	/* power down */
	int32_t wyn = wpisz_do_rejestru(ADRES_HIGROMETRU,
	                            CTRL_REG1_HIGROMETRU,
	                            0x00);
	if (wyn != SUKCES) {
		usart_nadaj_blad("nie mozna wylaczyc higrometru I2C1->SR1 =",
		                 wyn);
	}
	return wyn;
}

static int32_t zainicjalizuj_higrometr(void)
{
	/* specjalnie puste */
	return wlacz_higrometr();
}

static int32_t zainicjalizuj_czujnik_oswietlenia(void)
{
	/* const uint8_t CONFIG_REG_ADDR = 0x0D; */
	/* const uint8_t CTRL_REG_ADDR = 0x0F; */
	/* const uint8_t E_REG_ADDR = 0x00; */
	/* wpisz_do_rejestru(ADRES_CZUJNIKA_OSWIETLENIA, E_REG_ADDR, 0x03); */
	return SUKCES;
}

static int32_t wlacz_barometr(void)
{
	return wpisz_do_rejestru(ADRES_BAROMETRU,
	                         CTRL_REG1_ADDR_BAROMETRU,
	                         0x80);
}

static int32_t wylacz_barometr(void)
{
	return wpisz_do_rejestru(ADRES_BAROMETRU,
	                         CTRL_REG1_ADDR_BAROMETRU,
	                         0x00);
}

static int32_t zainicjalizuj_barometr(void)
{
	/* barometr jest od razu wylaczony */
	return SUKCES;
}

static int32_t zainicjalizuj_termometr(void)
{
	/* specjalnie puste - on jest juz wylaczony */
	return SUKCES;
}


int32_t podaj_wilgotnosc(int16_t *const wilg)
{
	const uint8_t
		H0_T0_OUT_ADDR = 0x36,
		H1_T0_OUT_ADDR = 0x3A,
		H0_rH_x2_ADDR = 0x30,
		H1_rH_x2_ADDR = 0x31,
		SR_ADDR = 0x27,
		HUMIDITY_OUT_L_ADDR = 0x28,
		HUMIDITY_OUT_H_ADDR = 0x29;
	const int16_t
		MAX_WILGOTNOSC = 100,
		MIN_WILGOTNOSC = 0;
	int to = PRZEDAWNIENIE;
	int8_t zaw_rej;
	bool koniec = false;
	int32_t err;
	/* wlacz_higrometr(); */
	err = wpisz_do_rejestru(ADRES_HIGROMETRU, CTRL_REG2_HIGROMETRU, 0x01);
	if (err != SUKCES) {
		usart_nadaj_blad("nie mozna ustawic higrometru na one shot, "
		                 "I2C1->SR1 = ", err);
	}
	while (!koniec) {
		to--;
		PODAJ_REJESTR_LUB_RETURN(ADRES_HIGROMETRU,
		                         SR_ADDR,
		                         &zaw_rej,
		                         "Nie mozna odczytac rejestru SR");
		koniec = (zaw_rej & 0x02) != 0;
		if (to == 0) {
			usart_nadaj_blad("przedawnienie z higro->sr",
			                 I2C1->SR1);
			return I2C1->SR1;
		}
	}
	uint8_t H0_rH_obl,
		H1_rH_obl;
	union {
		int8_t pojedyncze[2];
		int16_t cale;
	} wyn, H0_T0_OUT, H1_T0_OUT;
	PODAJ_REJESTR_LUB_RETURN(ADRES_HIGROMETRU,
	                         H0_T0_OUT_ADDR,
	                         &(H0_T0_OUT.pojedyncze[0]),
	                         "nie można wziąć H0_T0_OUT");
	PODAJ_REJESTR_LUB_RETURN(ADRES_HIGROMETRU,
	                         H0_T0_OUT_ADDR + 1,
	                         &(H0_T0_OUT.pojedyncze[1]),
	                         "nie można wziąć H0_T0_OUT");
	PODAJ_REJESTR_LUB_RETURN(ADRES_HIGROMETRU,
	                         H1_T0_OUT_ADDR,
	                         &(H1_T0_OUT.pojedyncze[0]),
	                         "nie można wziąć H1_T0_OUT");
	PODAJ_REJESTR_LUB_RETURN(ADRES_HIGROMETRU,
	                         H1_T0_OUT_ADDR + 1,
	                         &(H1_T0_OUT.pojedyncze[1]),
	                         "nie można wziąć H1_T0_OUT");
	PODAJ_REJESTR_LUB_RETURN(ADRES_HIGROMETRU,
	                         H0_rH_x2_ADDR,
	                         (int8_t *) &H0_rH_obl,
	                         "nie można wziąć H0_rH_obl");
	H0_rH_obl /= 2;
	PODAJ_REJESTR_LUB_RETURN(ADRES_HIGROMETRU,
	                         H1_rH_x2_ADDR,
	                         (int8_t *) &H1_rH_obl,
	                         "nie można wziąć H1_rH_obl");
	H1_rH_obl /= 2;
	PODAJ_REJESTR_LUB_RETURN(ADRES_HIGROMETRU,
	                         HUMIDITY_OUT_L_ADDR,
	                         &(wyn.pojedyncze[0]),
	                         "nie mozna wziac pojedyncze[0] z higrometru");
	PODAJ_REJESTR_LUB_RETURN(ADRES_HIGROMETRU,
	                         HUMIDITY_OUT_H_ADDR,
	                         &(wyn.pojedyncze[1]),
	                         "nie mozna wziac pojedyncze[0] z higrometru");
	/* wylacz_higrometr(); */
	*wilg = (H0_rH_obl + (wyn.cale - H0_T0_OUT.cale) * (H1_rH_obl - H0_rH_obl) /
	         (H1_T0_OUT.cale - H0_T0_OUT.cale));
	if (*wilg > MAX_WILGOTNOSC) {
		*wilg = MAX_WILGOTNOSC;
	} else if (*wilg < MIN_WILGOTNOSC) {
		*wilg = MIN_WILGOTNOSC;
	}
	return SUKCES;
}

int32_t podaj_cisnienie(int32_t *const cis)
{
	const uint8_t CTRL_REG2_ADDR = 0x21;
	const uint8_t SR_REG_ADDR = 0x27;
	const uint8_t PRESS_XL_REG_ADDR = 0x28;
	const uint8_t PRESS_L_REG_ADDR = 0x29;
	const uint8_t PRESS_H_REG_ADDR = 0x2A;
	union {
		int8_t pojedyncze[4];
		int32_t wynik;
	} wyn;
	int8_t sr;
	int to = PRZEDAWNIENIE;
	wlacz_barometr();
	wpisz_do_rejestru(ADRES_BAROMETRU, CTRL_REG2_ADDR, 0x01); /* one shot */
	do {
		PODAJ_REJESTR_LUB_RETURN(ADRES_BAROMETRU,
		                         SR_REG_ADDR,
		                         &sr,
		                         "Nie można odczytać SR z barometru");
		to--;
	} while ((sr & 0x02) == 0 && to > 0);
	if (to == 0) {
		wylacz_barometr();
		return TIMEOUT_ERR;
	}
	wyn.pojedyncze[3] = 0;
	PODAJ_REJESTR_LUB_RETURN(ADRES_BAROMETRU,
	                         PRESS_H_REG_ADDR,
	                         &(wyn.pojedyncze[2]),
	                         "nie mozna wziac PRESS_H_REG z barometru");
	PODAJ_REJESTR_LUB_RETURN(ADRES_BAROMETRU,
	                         PRESS_L_REG_ADDR,
	                         &(wyn.pojedyncze[1]),
	                         "nie mozna wziac PRESS_L_REG z barometru");
	PODAJ_REJESTR_LUB_RETURN(ADRES_BAROMETRU,
	                         PRESS_XL_REG_ADDR,
	                         &(wyn.pojedyncze[0]),
	                         "nie mozna wziac PRESS_XL_REG z barometru");;
	if ((wyn.pojedyncze[2] & 0x80) == 0) {
		wyn.pojedyncze[3] = 0xFF;
	} else {
		wyn.pojedyncze[2] &= ~(1 << 7);
	}
	wylacz_barometr();
	*cis = wyn.wynik / 4096 / 2;
	return SUKCES;
}

int32_t podaj_oswietlenie(int32_t *const osw)
{
	/* const uint8_t C0DATA_REG_ADDR = 0x14; */
	/* const uint8_t C0DATAH_REG_ADDR = 0x15; */
	/* const uint8_t C1DATA_REG_ADDR = 0x16; */
	/* const uint8_t C1DATAH_REG_ADDR = 0x17; */
	/* const uint8_t CPL = (ATIME_ms * AGAINx) / (GA * 60); */
	/* const uint8_t Lux1 = (C0DATA - (187 * C1DATA) / 100) / CPL; */
	/* const uint8_t Lux2 = ((63 * C0DATA) / 100 - C1DATA) / CPL; */
	/* const uint8_t wynik = MAX(Lux1, Lux2, 0); */
	return SUKCES;
}

int32_t zainicjalizuj_peryferia(void)
{
	zainicjalizuj_higrometr();
	zainicjalizuj_czujnik_oswietlenia();
	zainicjalizuj_barometr();
	zainicjalizuj_termometr();
	return SUKCES;
}

static int32_t wlacz_termometr(void)
{
	return wpisz_do_rejestru(ADRES_TERMOMETRU,
	                         ADRES_REJESTRU_CONF_TEMP,
	                         0x00);
}

static int32_t wylacz_termometr(void)
{
	return wpisz_do_rejestru(ADRES_TERMOMETRU,
	                         ADRES_REJESTRU_CONF_TEMP,
	                         0x01);
}

int32_t podaj_temperature(int16_t *const temp)
{
	const uint8_t ADRES_REJESTRU_Z_TEMPERATURA = 0;
	int
		to = PRZEDAWNIENIE,
		delay = 100000;
	int16_t wynik;
	wlacz_termometr();
	/* trzeba poczekac 150ms na pomiar */
	while (delay--) {
		__NOP();
	}
	do {
		PODAJ_DWA_BAJTY_LUB_RETURN(ADRES_TERMOMETRU,
		                           ADRES_REJESTRU_Z_TEMPERATURA,
		                           &wynik,
		                           "nie można pobrać temperatury");
		to--;
	} while (wynik == 0 && to > 0);
	wylacz_termometr();
	if (to == 0) {
		return TIMEOUT_ERR;
	}
	if (wynik < 0) {
		wynik = ((wynik * -1) >> 8) * -1;
	} else {
		wynik = wynik >> 8;
	}
	*temp = wynik;
	return SUKCES;
}
