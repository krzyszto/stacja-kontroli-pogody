#ifndef _OBSLUGA_CZUJNIKOW_H
#define _OBSLUGA_CZUJNIKOW_H

#include <stdint.h>

extern int32_t zainicjalizuj_peryferia(void);

extern int32_t podaj_wilgotnosc(int16_t *const wilg);
extern int32_t podaj_cisnienie(int32_t *const cis);
extern int32_t podaj_oswietlenie(int32_t *const osw);
extern int32_t podaj_temperature(int16_t *const temp);

#endif
