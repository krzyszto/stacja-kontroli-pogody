#ifndef USART_H_KRZYSZTOF_
#define USART_H_KRZYSZTOF_
#include <stdint.h>

extern void usart_ustaw(void);

extern void usart_nadajbeznl(char *tekst);

extern void usart_nadaj(char *tekst);

extern void usart_nadaj_liczbe_32(int32_t liczba);

extern void ustaw_przerwania_usarta(void);

extern void nadaj_znak(char znak);

extern void usart_nadaj_blad(char *tekst, int32_t kod_bledu);

extern void usart_debug(char *tekst, int32_t wartosc);

#endif
